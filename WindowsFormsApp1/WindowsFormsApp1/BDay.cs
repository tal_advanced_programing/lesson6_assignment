﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

namespace WindowsFormsApp1
{
    public partial class BDay : Form
    {
        private string  username;
        public BDay(string user)
        {
            InitializeComponent();
            username = user;
        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
             label1.Text = monthCalendar1.SelectionRange.Start.ToShortDateString();
           
            
            string date;
            int comaIndex;

            int firstSlesh;
            int secondSlesh;
            string day, month, year;
            string name;
            string path = (username + "BD.txt");
            string selectionDate = monthCalendar1.SelectionRange.Start.ToShortDateString();

            secondSlesh = selectionDate.LastIndexOf('/');
            selectionDate = selectionDate.Substring(0, secondSlesh);


            if (!(System.IO.File.Exists(path)))
            {
                var myFile = System.IO.File.Create(path);
                myFile.Close();
            }
            else
            {
                string[] data = System.IO.File.ReadAllLines(path);

                foreach (string line in data)
                {
                    comaIndex = line.IndexOf(',');
                    name = line.Substring(0, comaIndex);
                    date = line.Substring(comaIndex + 1);

                    firstSlesh = date.IndexOf('/');
                    secondSlesh = date.LastIndexOf('/');

                    month = date.Substring(0, firstSlesh);
                    day = date.Substring(firstSlesh + 1, secondSlesh - firstSlesh - 1);
                    year = date.Substring(secondSlesh + 1);

                    if(day.Length == 1)
                    {
                        day = '0' + day;
                    }
                    if(month.Length == 1)
                    {
                        month = '0' + month;
                    }

                    date = day + '/' + month;
                    if(selectionDate.Equals(date))
                    {
                        label1.Text = name + " has a BD";
                        break;
                    }
                    else
                    {
                        label1.Text = "No one have a BD";
                    }

                   
                }
            }
        }
    }
}
