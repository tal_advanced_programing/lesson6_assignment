﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void cancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ok_Click(object sender, EventArgs e)
        {
            string[] lines = System.IO.File.ReadAllLines("Users.txt");
            bool flag = false;
            int comaIndex;
            string username, password;
            foreach (string line in lines)
            {
                comaIndex = line.IndexOf(',');
                username = line.Substring(0, comaIndex);
                password = line.Substring(comaIndex + 1);

                if (username.Equals(textBox1.Text) && password.Equals(textBox2.Text))
                {
                    this.Hide();
                    Form BD = new BDay(username);
                    BD.ShowDialog();
                    flag = true;
                }
            }

            if (!flag)
            {
                MessageBox.Show("UserName or Password is incorect", "Error!");
            }
        }
    }
}
